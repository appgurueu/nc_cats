-- LUALOCALS < ---------------------------------------------------------
local include, minetest, rawset
    = include, minetest, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
rawset(_G, modname, {})

include("names")
include("node")
include("migrate")
include("item")
include("ore")
include("craft")
include("pet")
include("wetconcrete")
include("ambiance")
include("hint")
