-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local myapi = _G[modname]

nodecore.register_ambiance({
		label = modname .. " ambiance",
		nodenames = {"group:" .. modname .. "_face"},
		interval = 1,
		chance = 25,
		check = myapi.notpurring,
		sound_name = modname .. "_mew",
		sound_gain = 0.5
	})

nodecore.register_item_ambiance({
		label = modname .. " item ambiance",
		itemnames = {"group:" .. modname .. "_cat"},
		interval = 1,
		chance = 25,
		sound_name = modname .. "_mew",
		sound_gain = 0.25
	})

nodecore.register_ambiance({
		label = modname .. " ore ambiance",
		nodenames = {"group:" .. modname .. "_ore"},
		interval = 1,
		chance = 100,
		sound_name = modname .. "_mew",
		sound_gain = 0.25,
		sound_pitch = 0.75
	})
