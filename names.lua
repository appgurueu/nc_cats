-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs
    = minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local myapi = _G[modname]

local modstore = minetest.get_mod_storage()

local names = {
	"Abby",
	"Apple Jack",
	"Archie",
	"Ash",
	"Bear",
	"Bella",
	"Belle",
	"Binx",
	"Blossom",
	"Bob",
	"Boo",
	"Boots",
	"Bubbles",
	"Buddy",
	"Buttercup",
	"Calvin",
	"Charlie",
	"Chloe",
	"Cleo",
	"Clover",
	"Clyde",
	"Coco",
	"Cokey",
	"Cricket",
	"Daisy",
	"Dottie",
	"Ducky",
	"Emma",
	"Felix",
	"Fluffy",
	"Frank",
	"Frankie",
	"Fuzzy",
	"Ginger",
	"Gizmo",
	"Gus",
	"Hazel",
	"Iris",
	"Jack",
	"Jackson",
	"Jax",
	"Joey",
	"Kiki",
	"Kitty",
	"Koosh",
	"Leo",
	"Lily",
	"Loki",
	"Lola",
	"Louie",
	"Lucky",
	"Lucy",
	"Luigi",
	"Lulu",
	"Maggie",
	"Marley",
	"Midnight",
	"Millie",
	"Milo",
	"Minnie",
	"Mittens",
	"Mochi",
	"Moose",
	"Murphy",
	"Nala",
	"Olivia",
	"Ollie",
	"Oreo",
	"Oscar",
	"Peanut",
	"Penny",
	"Pepper",
	"Piper",
	"Pixie",
	"Polly",
	"Poppy",
	"Princess",
	"Pumpkin",
	"Riley",
	"Rocky",
	"Romeo",
	"Rose",
	"Rosie",
	"Ruby",
	"Salem",
	"Sam",
	"Scout",
	"Shadow",
	"Simba",
	"Sophie",
	"Sunny",
	"Sunshine",
	"Teddy",
	"Thor",
	"Tiger",
	"Tigger",
	"Toby",
	"Tucker",
	"Tux",
	"Willow",
}

local nameidx = {}
for _, v in pairs(names) do
	nodecore.translate_inform(v)
	nameidx[v] = modstore:get_int(v) or 0
end

local desc = "description"

myapi.setname = function(meta)
	local name = meta:get_string(desc)
	if (not name) or name == "" then

		-- Guard against thousands of cats being created, causing math overflow
		local min = 0
		for _, v in pairs(nameidx) do
			if v > min then min = v end
		end

		local _
		_, name = nodecore.pickrand(nameidx, function(v) return 0.5 ^ (v - min) end)

		nameidx[name] = nameidx[name] + 1
		modstore:set_int(name, nameidx[name])
		meta:set_string(desc, name)
	end
	return name
end
