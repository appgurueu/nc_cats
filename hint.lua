-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

nodecore.register_hint("find cat ore",
	"group:" .. modname .. "_ore",
	"toolcap:cracky:2"
)

nodecore.register_hint("dig cat cobble",
	{true, "dig:" .. modname .. ":cobble", "dig:" .. modname .. ":cobble_loose"},
	"group:" .. modname .. "_ore"
)

nodecore.register_hint("incubate cat cobble into a cocoon",
	"cat mature",
	{true, "dig:" .. modname .. ":cobble", "dig:" .. modname .. ":cobble_loose"}
)

nodecore.register_hint("hatch a cat from a cocoon",
	"hatch cat",
	"cat mature"
)

nodecore.register_hint("pet a cat",
	"pet cat",
	"group:" .. modname .. "_cat"
)

nodecore.register_hint("get a furball from a cat",
	"cat eject prill",
	"pet cat"
)

nodecore.register_hint("insert a cat prill into cobble",
	"cat renew",
	{modname .. ":prill", "nc_lux:cobble8"}
)
