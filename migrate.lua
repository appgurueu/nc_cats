-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local myapi = _G[modname]

local oldname = "catnode:cat"

if minetest.registered_nodes[oldname] then return end

minetest.register_node(
	":" .. oldname,
	{pointable = false, drawtype = "airlike"}
)

minetest.register_abm({
		label = modname .. ":migrate",
		nodenames = {oldname},
		interval = 1,
		chance = 1,
		action = function(pos, node)
			myapi.makecat(pos, node.param2)
		end
	})
minetest.register_lbm({
		name = modname .. ":migrate",
		nodenames = {oldname},
		run_on_every_load = true,
		action = function(pos, node)
			myapi.makecat(pos, node.param2)
		end
	})
